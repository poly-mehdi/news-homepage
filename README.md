# Frontend Mentor - News homepage solution

This is a solution to the [News homepage challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/news-homepage-H6SWTa1MFl). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page

### Screenshot

![Mobile Design](./design/mobile-design.png)
![Desktop Design](./design/desktop-design.png)
![Sidebar Design](./design/sidebar-design.png)

### Links

- Solution URL: [News Homepage](https://news-homepage-mehdi.vercel.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow

### What I learned

J'ai eu l'opportunité d'apprendre à utiliser le Grid en CSS lors de la réalisation de ce projet. Grâce à cette fonctionnalité puissante, j'ai pu créer des mises en page complexes et réactives pour mon site web. Le Grid m'a permis d'organiser facilement mes éléments en utilisant des grilles bidimensionnelles, en spécifiant les zones et en les alignant de manière précise. J'ai découvert comment définir les modèles de grille, les espaces entre les éléments, ainsi que les propriétés de positionnement. Grâce à cette compétence, j'ai pu obtenir un contrôle total sur la mise en page de mon site, en créant des mises en page flexibles et esthétiques. L'utilisation du Grid en CSS a considérablement amélioré mes compétences en développement web et a ouvert de nouvelles possibilités pour mes projets futurs.

## Author

- Website - [Mon Portofolio](https://portofolio-poly-mehdi.vercel.app/)
- Frontend Mentor - [@poly-mehdi](https://www.frontendmentor.io/profile/poly-mehdi)
